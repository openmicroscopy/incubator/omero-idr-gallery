import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransitionGroup } from 'react-transition-group';
import ObjectLink from './ObjectLink';
import Header from '../header/Header';
import connect from '../fetch/connect';
import { BASE_URL } from '../fetch/constants';

const comp = (a, b) => {
  const nameA = a.Name;
  const nameB = b.Name;
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }
  // names must be equal
  return 0;
};

const Containers = ({ screens, projects }) => (
  <div>
    <Header />
    <div className="containerContainer">
      <ul>
        {screens.pending && (
          <li
            index={0}
          >
            <span>
              Loading...
            </span>
          </li>
        )}
        <CSSTransitionGroup
          className="cssGroup"
          transitionName="example"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={0}
        >
          {screens.rejected && (
            <li key="rejected">
              {screens.reason}
            </li>
          )}

          {projects.rejected && (
            <li key="rejected">
              {screens.reason}
            </li>
          )}

          {screens.fulfilled && projects.fulfilled && (

            projects.value.data.concat(screens.value.data)
              .sort(comp)
              .map((p, i) => (
                <li
                  key={p.Name + p['@id']}
                  style={{ transitionDelay: `${(i * 0.02)}s` }}
                  className="underline"
                >
                  <ObjectLink
                    datatype={p['@type'].split('#')[1].toLowerCase()}
                    id={p['@id']}
                  >
                    {p.Name}
                  </ObjectLink>
                </li>
              ))
          )}
        </CSSTransitionGroup>
      </ul>
    </div>
  </div>
);

Containers.propTypes = {
  // see https://github.com/heroku/react-refetch#example
  screens: PropTypes.shape({
    values: PropTypes.array,
    pending: PropTypes.bool,
    rejected: PropTypes.bool,
    reason: PropTypes.string,
    fulfilled: PropTypes.bool,
  }).isRequired,
  projects: PropTypes.shape({
    values: PropTypes.array,
    pending: PropTypes.bool,
    rejected: PropTypes.bool,
    reason: PropTypes.string,
    fulfilled: PropTypes.bool,
  }).isRequired,
};


const withScreens = connect(() => ({
  screens: `${BASE_URL}/api/v0/m/screens/`,
  projects: `${BASE_URL}/api/v0/m/projects/`,
}));
export default withScreens(Containers);
