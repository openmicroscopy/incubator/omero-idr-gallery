import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Screen from './containers/Screen';
import Plates from './containers/Plates';
import Plate from './containers/Plate';
import Project from './containers/Project';
import Datasets from './containers/Datasets';
import Dataset from './containers/Dataset';
import Images from './containers/Images';
import Image from './containers/Image';
import ScreensAndProjects from './containers/ScreensAndProjects';

const App = () => (
  <Router>
    <div className="App">
      <Route
        exact
        path="/"
        render={props => (
          <ScreensAndProjects {...props} />
        )}
      />
      <Route
        exact
        path="/screens/:id/"
        render={props => (
          <Screen {...props} datatype="screen" />
        )}
      />
      <Route
        exact
        path="/screens/:id/plates/"
        render={props => (
          <Plates {...props} datatype="plate" />
        )}
      />
      <Route
        exact
        path="/plates/:id/"
        render={props => (
          <Plate {...props} datatype="plate" />
        )}
      />
      <Route
        exact
        path="/projects/:id/"
        render={props => (
          <Project {...props} datatype="project" />
        )}
      />
      <Route
        exact
        path="/projects/:id/datasets/"
        render={props => (
          <Datasets {...props} datatype="dataset" />
        )}
      />
      <Route
        exact
        path="/datasets/:id/"
        render={props => (
          <Dataset {...props} datatype="dataset" />
        )}
      />
      <Route
        exact
        path="/datasets/:id/images/"
        render={props => (
          <Images {...props} datatype="image" />
        )}
      />
      <Route
        exact
        path="/images/:id/"
        render={props => (
          <Image {...props} datatype="image" />
        )}
      />
    </div>
  </Router>
);

export default App;
