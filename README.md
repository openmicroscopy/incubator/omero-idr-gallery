
Prototype React.js project to browse [IDR](http://idr.openmicroscopy.org/)
data using the OMERO.web JSON api.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

